import { Col, Container, Row } from "react-bootstrap";
import NavBar from "./components/navBar";
import FormCrear from "./components/formCrear";
import React, { useState, useEffect } from "react";
import TableList from "./components/tableList";

const App = () => {
  const [registros, setregistros] = useState([]);
  const [getFlujoCaja, setFlujoCaja] = useState();
  const [getSaldoFinalTemporal, setSaldoFinalTemporal] = useState();

  const addRegistro = (registro) => {
    setregistros([...registros, registro]);
  };

  const removeRegistro = (id) => {
    setregistros(registros.filter((registro) => registro.id !== id));
  };

  const editRegistro = (id, newregistro) => {
    const newRegistroEdit = registros.map((registro) => {
      if (registro.id === id) {
        return { ...registro, name: newregistro.name, count: newregistro.count, type: newregistro.type };
      }
      return registro;
    });
    setregistros(newRegistroEdit);
  };

  const calcularFlujoCaja = () => {

    var acum = 0
    registros.map((registro) => {

      if (registro.type === "Gasto") {
        registro.count = parseInt(registro.count) * -1
      }

      acum = parseInt(acum) + parseInt(registro.count)

    })
    setFlujoCaja(acum)
  }

  useEffect(() => {
    calcularFlujoCaja()
  }, [registros])

  const validadorDeSaldoFinalFuncion = (saldoFinalTemporal) => {
    setSaldoFinalTemporal(saldoFinalTemporal)
  }

  const formatoMoneda = (numero) => {
    let formato = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'COP'
    })

    return formato.format(numero)
  }

  return (
    <Container>
      <NavBar 
        flujoCaja={getFlujoCaja} 
        validadorDeSaldoFinalFuncion={validadorDeSaldoFinalFuncion} 
        formatoMoneda={formatoMoneda} />
      <br />
      <Row>
        <Col>
          <FormCrear 
            addRegistro={addRegistro} 
            saldoFinalActualFuncion={getSaldoFinalTemporal} />
        </Col>
        <Col>
          <TableList
            registros={registros}
            removeRegistro={removeRegistro}
            editRegistro={editRegistro}
            formatoMoneda={formatoMoneda}
          />
        </Col>
      </Row>
    </Container>
  );
};
export default App;