export const validaciones = 
    {
        tipos:{
            validacionError: "Notificacion de error",
            validacionExito: "Notificacion de exito"
        },error: {
            campoNombre: 'Se debe ingresar el campo nombre',
            campoCantidad: 'Se debe ingresar el campo cantidad',
            campoCantidadMayorACero: 'El campo cantidad debe ser mayor a cero',
            campoCantidadNumerico: 'El campo cantidad debe numerico',
            saldoDisponibleGasto: 'No tienes saldo disponible para hacer el movimiento Gasto',
        },
        exito:  ' creado con exito'
    }

