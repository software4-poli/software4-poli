import React, { useEffect, useState } from "react";
import {
  Table,
  Row,
  Col,
  Container,
  Form,
  Badge,
  Button, InputGroup
} from "react-bootstrap";

import { RiDeleteBin2Line } from "react-icons/ri";
import ModalModificar from "./modalModificar";

const TableList = ({ registros, removeRegistro, editRegistro,formatoMoneda }) => {
  const [getRegistrosFiltrados, setRegistrosFiltrados] = useState()

  const handleChangeKeyWord = (e) => {

    let registrosFil = registros.filter(item =>
      item.name.toLowerCase().includes(e.target.value.toLowerCase())
    );

    setRegistrosFiltrados(registrosFil)
  }

  useEffect(() => {
    setRegistrosFiltrados(registros)
  }, [registros])

  const handleClick = (e) => {

    let registrosFil = registros.filter(item => item.type == e.target.value)

    if (e.target.value === "Ingreso" | e.target.value === "Gasto") {
      setRegistrosFiltrados(registrosFil)
    } else {
      setRegistrosFiltrados(registros)
    }
  }

  return (
    <Container>
      <Row>
        <Col>
          <Form.Label column sm={10}>
            Lista de moviemientos
          </Form.Label>
          <Badge variant="primary">{registros.length}</Badge>
        </Col>
      </Row>
      <Row>
        <Col>
          <InputGroup className="mb-3 mt-3">
            <Form.Control
              type="text"
              name="name"
              autoComplete="off"
              placeholder="Buscar por nombre de movimiento"
              onChange={handleChangeKeyWord}
            />
          </InputGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <InputGroup className="mb-3 mt-3">
            <Form.Label>Todos</Form.Label>
            <Form.Control
              type="radio"
              name="radios"
              value="Todos"
              onChange={handleClick}
              placeholder="Todos"/>
            <Form.Label>Ingreso</Form.Label>
            <Form.Control
              type="radio"
              name="radios"
              value="Ingreso"
              onChange={handleClick}
              placeholder="Ingreso"
            />
            <Form.Label>Gasto</Form.Label>
            <Form.Control
              type="radio"
              name="radios"
              value="Gasto"
              onChange={handleClick}
              placeholder="Gasto"
            />
          </InputGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table>
            <tbody>
              {getRegistrosFiltrados?.map((item) => (
                <tr key={item.id}>
                  <td>
                    <ModalModificar registros={item} editRegistro={editRegistro} />
                    <Button
                      onClick={() => {
                        removeRegistro(item.id);
                      }}
                      variant="primary"
                    >
                      <RiDeleteBin2Line />
                    </Button>
                  </td>
                  <td>{item.name}</td>
                  <td>
                    <Badge variant={item.type === 'Ingreso' ? "success" : "danger"}>{formatoMoneda(item.count)}</Badge>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
};

export default TableList;