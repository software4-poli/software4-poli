import React, {useState} from 'react';
import {Button,Modal,Row,Col,Form} from "react-bootstrap";
import { FiEdit2 } from "react-icons/fi";

const ModalModificar = ({registros, editRegistro}) => {
    const [show, setShow] = useState(false);

    const handleClose = () => {
        setShow(false)
        editRegistro (registros.id,registroEdit)
    };
    const handleShow = () => setShow(true);

    const [registroEdit, setRegistroEdit] = useState(registros);

    const handleImputChange = (e) => {
        setRegistroEdit({ registros, [e.target.name]: e.target.value });
      };

    return (
        <>
          <Button variant="primary" onClick={handleShow}>
          <FiEdit2 />
          </Button>
    
          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Editar registro</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Form.Group as={Row} controlId="formGridState">
            <Form.Label column sm={4}>
              Tipo Movimiento
            </Form.Label>
            <Col sm={8}>
              <Form.Control
                as="select"
                type="text"
                placeholder=""
                name="type"
                value={registroEdit.type}
                onChange={handleImputChange}
              >
                <option>Seleccionar</option>
                <option>Ingreso</option>
                <option>Gasto</option>
              </Form.Control>
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="formNombre">
            <Form.Label column sm={2}>
              Nombre
            </Form.Label>
            <Col sm={10}>
              <Form.Control
                type="text"
                placeholder=""
                name="name"
                value={registroEdit.name}
                onChange={handleImputChange}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formCantidad">
            <Form.Label column sm={2}>
              Cantidad
            </Form.Label>
            <Col sm={10}>
              <Form.Control
                type="text"
                placeholder=""
                name="count"
                value={registroEdit.count}
                onChange={handleImputChange}
              />
            </Col>
          </Form.Group>
                </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Cerrar
              </Button>
              <Button variant="primary" onClick={handleClose}>
                Guardar cambios
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      );
    
}

export default ModalModificar
