import React, { useEffect, useState } from "react";
import { Navbar, Form } from "react-bootstrap";

export const NavBar = ({ flujoCaja, validadorDeSaldoFinalFuncion, formatoMoneda }) => {

  const [getSaldoFinal, setSaldoFinal] = useState(0);
  const [getSaldoInicial, setSaldoInicial] = useState(0);

  const handleChange = (e) => {

    const saldoInicial = parseInt(e.target.value)

    if(flujoCaja == 0){
      if(isNaN(saldoInicial)){ 
        setSaldoFinal(0)
      }else{
        setSaldoFinal(saldoInicial)
      }
    }else{
      if(isNaN(saldoInicial)){
        setSaldoFinal(flujoCaja)
      }else{
        setSaldoFinal(saldoInicial + parseInt(flujoCaja))
      }
    }
    setSaldoInicial(saldoInicial)
  }

  useEffect(() => {
      console.log("getSaldoFinal: ",getSaldoFinal,"getFlujoCaja: ",flujoCaja)
      setSaldoFinal((parseInt(getSaldoInicial) + parseInt(flujoCaja)))
  }, [flujoCaja])

  useEffect(() => {
    validadorDeSaldoFinalFuncion(getSaldoFinal)
}, [getSaldoFinal])

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand href="#home">
        <img
          alt=""
          src="https://i.pinimg.com/736x/c1/7a/75/c17a7540e1c301e7648e242bf0c0a5ab.jpg"
          width="30"
          height="30"
          className="d-inline-block align-top"
        />{" "}
        Presupuesto
      </Navbar.Brand>
      <Navbar.Toggle />
      <Navbar.Collapse className="justify-content-end">
        <Form inline>
          <Form.Label className="mr-sm-2">Saldo Inicial:</Form.Label>
          <Form.Control type="text" onChange={handleChange} placeholder="" className="mr-sm-2" name="saldoInicial" />
          <Form.Label className="mr-sm-2" >Saldo Final:</Form.Label>
          <Form.Control type="text" placeholder="" className="mr-sm-2" value={formatoMoneda(getSaldoFinal)} readOnly />
        </Form>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavBar;