import React, { useEffect, useState } from 'react';
import { Button, Modal, Row, Col, Form } from "react-bootstrap";
import { FiEdit2 } from "react-icons/fi";

const ModalNotificaciones = ({ mostrar, tipo, mensaje, cerrar }) => {

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {

        console.log(mostrar)
        if (mostrar) {
            handleShow()
        } else {
            handleClose()
        }
    }, [mostrar])

    const handlerClose = () => {
        handleClose()
        cerrar(false)
    }

    return (
        <>
            <Modal 
            show={show} 
            onHide={handleClose}
            >
                <Modal.Header closeButton>
                    <Modal.Title> {tipo}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group as={Row} controlId="formGridState">
                        <Col sm={12}>
                            {mensaje}
                        </Col>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={handlerClose}>
                        Cerrar
                </Button>
                </Modal.Footer>
            </Modal>
        </>
    );

}

export default ModalNotificaciones