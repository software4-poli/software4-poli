import { Button, Container, Row, Col, Modal, Form } from "react-bootstrap";
import "../style/styles.css";
import React, { useEffect, useState } from "react";
import { v4 as uuid_v4 } from "uuid";
import ModalNotificaciones from "./modalNotificaciones";
import { validaciones } from "./../validacion/validaciones";

const FormCrear = ({ addRegistro,saldoFinalActualFuncion }) => {

  const [getShowModal, setShowModal] = useState(false)
  const [getType, setType] = useState()
  const [getMessage, setMenssage] = useState()
  const [registro, setregistro] = useState({
    id: "",
    name: "",
    count: "",
    type: "",
  });

  useEffect(() => {
    setregistro({ ...registro, "type": "Ingreso" })
  }, [])

  const handleImputChange = (e) => {
    setregistro({ ...registro, [e.target.name]: e.target.value })
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    var mensajeError = ""
    if (registro.name.length == 0) {
      mensajeError = validaciones.error.campoNombre
    }

    if (registro.count.length == 0) {
      if (mensajeError === "") {
        mensajeError = validaciones.error.campoCantidad
      } else {
        mensajeError = mensajeError + " - " + validaciones.error.campoCantidad
      }
    } else {

      if (isNaN(parseInt(registro.count))) {
        if (mensajeError === "") {
          mensajeError = validaciones.error.campoCantidadNumerico
        } else {
          mensajeError = mensajeError + " - " + validaciones.error.campoCantidadNumerico
        }
      } else {
        if (registro.count <= 0) {
          if (mensajeError === "") {
            mensajeError = validaciones.error.campoCantidadMayorACero
          } else {
            mensajeError = mensajeError + " - " + validaciones.error.campoCantidadMayorACero
          }
        }
      }
    }

    if(saldoFinalActualFuncion <= 0 && registro.type === "Gasto"){
      if (mensajeError === "") {
        mensajeError = validaciones.error.saldoDisponibleGasto
      } else {
        mensajeError = mensajeError + " - " + validaciones.error.saldoDisponibleGasto
      }
    }

    if (mensajeError === "") {
      addRegistro({ ...registro, id: uuid_v4() })
      setregistro({ ...registro, name: "", count: "", type: "Ingreso" })
      dataModal(validaciones.tipos.validacionExito, registro.type+validaciones.exito)
    } else {
      dataModal(validaciones.tipos.validacionError, mensajeError)
    }
  };

  const handleCancelar = (e) => {
    e.preventDefault();
    //Reset form
    setregistro({ ...registro, name: "", count: "", type: "Ingreso" })
  };

  const closeModal = (status) => {
    setShowModal(status);
  }

  const dataModal = (tipo, mensaje) => {
    setShowModal(true);
    setMenssage(mensaje)
    setType(tipo)
  }
  return (
    <Container>
      <Form.Group as={Row} controlId="formGridState">
        <Form.Label column sm={6}>
          Registro
        </Form.Label>
      </Form.Group>
      <Form.Group as={Row} controlId="formGridState">
        <Form.Label column sm={4}>
          Tipo Movimiento
        </Form.Label>
        <Col sm={8}>
          <Form.Control
            as="select"
            name="type"
            value={registro.type}
            onChange={handleImputChange}
          >
            <option>Ingreso</option>
            <option>Gasto</option>
          </Form.Control>
        </Col>
      </Form.Group>
      <ModalNotificaciones mostrar={getShowModal} tipo={getType} mensaje={getMessage} cerrar={closeModal} />
      <Form.Group as={Row} controlId="formNombre">
        <Form.Label column sm={2}>
          Nombre
        </Form.Label>
        <Col sm={10}>
          <Form.Control
            type="text"
            placeholder=""
            autoComplete="off"
            name="name"
            value={registro.name}
            onChange={handleImputChange}
          />
        </Col>
      </Form.Group>
      <Form.Group as={Row} controlId="formCantidad">
        <Form.Label column sm={2}>
          Cantidad
        </Form.Label>
        <Col sm={10}>
          <Form.Control
            type="text"
            placeholder=""
            autoComplete="off"
            name="count"
            value={registro.count}
            onChange={handleImputChange}
          />
        </Col>
      </Form.Group>
      <Form.Group as={Row} controlId="formCantidad">
        <Col sm={5}>
          <Button onClick={handleCancelar} variant="primary">Cancelar</Button>
        </Col>
        <Col sm={5}>
          <Button onClick={handleSubmit} variant="primary">
            Agregar Movimiento
      </Button>
        </Col>
      </Form.Group>
    </Container>
  );
};

export default FormCrear;
